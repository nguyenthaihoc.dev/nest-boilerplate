## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.
Using node version >=v18

## Run project
### 1.Prepare
```bash
$ cp .env.example .env
```
### 2. Config info in file .env

### 3. Installation
```bash
$ yarn
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# debug mode
$ npm run start:debug

# production mode
$ npm run start:prod
```

## Directory Structure


### Detailed Directory & File Descriptions

- `src`: Main source code directory for the application.
    - `config`: Contains configuration files.
    - `constants`: Holds constant values used throughout the application like `env.constant.ts` and `error.constant.ts`.
    - `infra`: This directory houses infrastructure related files, handling exceptions and others.
    - `models`: Contains data models.
      - `schemas`: Contains schemas for data models.
      - `repositories`: Contains repositories for data models.
    - `modules`: The directory contains system module
      - `dto`: Data Transfer Objects (DTOs) are the basis of data validation in NestJS applications. DTOs enable various layers of flexible data validation in NestJS.
      - `controllers`: Controllers are responsible for handling incoming requests and returning responses to the client.
      - `services`: Services are responsible for processing incoming requests from the controller and returning a response to the controller.
      - `modules`: Contains modules for the application.
      - `helpers`: Contains helper functions (if needed)
    - `shared`: This directory consists of resources or files that are shared among different parts of the application.

- It is possible to create a module, following nestjs DI principles. For example, create a notification module to handle functions related to notification service.
- The `src/modules/template` directory is a sample code directory for reference and study
- Structure of a module `modules`:


### Rule
- All modules must be registered in `app.module.ts`
- Name folders and files using `-` as separator. For example: filter-template.dto.ts
- Controller is only for receiving requests and declaring dto, not processing logic
- Service handles computational logic
- Repository handles queries, searches, and interactions with the database
- Schema declares the MongoDB schema
- ErrorCode will be defined later