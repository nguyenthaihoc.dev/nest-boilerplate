import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';

import { EEnvKey } from '@constants/env.constant';

import { useMorgan } from '@infra/middleware';
import { HttpExceptionFilter } from '@infra/middleware/http-exception.filter';
import { UnknownExceptionsFilter } from '@infra/middleware/unknown-exceptions.filter';
import { LoggerService } from '@shared/modules/loggers/logger.service';
import { BodyValidationPipe } from '@infra/pipes/validation.pipe';
import { initSwagger } from '@shared/swagger';

import { AppModule } from './app.module';
import { ResponseTransformInterceptor } from '@infra/interceptors/request-response.interceptor';
import { ClsMiddleware } from 'nestjs-cls';
import { nanoid } from 'nanoid';
import { SetupBullBoard } from '@infra/bull/bull-board';

async function bootstrap() {
    const loggingService = new LoggerService();
    const app = await NestFactory.create<NestExpressApplication>(AppModule, { logger: loggingService.logger.default });
    const configService = app.get(ConfigService<any>);
    app.use(
        new ClsMiddleware({
            generateId: true,
            idGenerator: (req: Request) => {
                const rid = req.headers['X-Request-Id'] ?? nanoid();
                req['rid'] = rid;
                return rid;
            },
        }).use,
    );
    const logger = loggingService.getLogger('Main');
    SetupBullBoard(app, configService);
    app.useGlobalInterceptors(new ResponseTransformInterceptor());
    app.useGlobalFilters(new UnknownExceptionsFilter(loggingService));
    app.useGlobalFilters(new HttpExceptionFilter(loggingService));

    app.useGlobalPipes(new BodyValidationPipe());
    app.setGlobalPrefix(configService.get<string>(EEnvKey.GLOBAL_PREFIX));
    app.enableCors();
    app.use(useMorgan(loggingService.logger.access));
    initSwagger(app, configService);
    await app.listen(configService.get<number>(EEnvKey.PORT) || 5000);
    logger.info(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
