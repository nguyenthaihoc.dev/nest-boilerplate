import { HttpException, Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { memoryStorage } from 'multer';

import { ConfigurationModule } from '@config/config.module';
import { DatabaseModule } from '@config/database.module';

import { LoggingModule } from '@shared/modules/loggers/logger.module';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MODULES } from './modules';
import { ClsModule } from 'nestjs-cls';
import { BullModule } from '@nestjs/bull';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EEnvKey } from '@constants/env.constant';
import { ModelModule } from '@models/model.module';
import { SentryInterceptor, SentryModule } from '@ntegral/nestjs-sentry';
import * as Sentry from '@sentry/node';
import { ProfilingIntegration } from '@sentry/profiling-node';
import { APP_INTERCEPTOR } from '@nestjs/core';

@Module({
    imports: [
        ModelModule,
        ClsModule.forRoot({
            global: true,
        }),
        BullModule.forRootAsync({
            imports: [ConfigurationModule],
            useFactory: async (config: ConfigService) => ({
                url: config.get(EEnvKey.REDIS_URL) || 'redis://localhost:6379',
            }),
            inject: [ConfigService],
        }),
        SentryModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (config: ConfigService) => ({
                dsn: config.get<string>('SENTRY_DSN', ''),
                environment: config.get<string>('NODE_ENV', 'development'),
                enabled: config.get<string>('SENTRY_DSN_IS_ENABLE', 'false') === 'true',
                integrations: [new Sentry.Integrations.Http({ tracing: true }), new ProfilingIntegration()],
                tracesSampleRate: 1.0,
                profilesSampleRate: 1.0,
            }),
            inject: [ConfigService],
        }),
        ConfigurationModule,
        DatabaseModule,
        LoggingModule,
        MulterModule.register({
            storage: memoryStorage(),
        }),
        ...MODULES,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        {
            provide: APP_INTERCEPTOR,
            useFactory: () =>
                new SentryInterceptor({
                    filters: [
                        {
                            type: HttpException,
                            filter: (exception: HttpException) => 500 > exception.getStatus(),
                        },
                    ],
                }),
        },
    ],
})
export class AppModule {}
