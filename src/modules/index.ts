import { TemplateModule } from '@modules/template/template.module';
import { JobSampleModule } from '@modules/job-sample/job-sample.module';

export const MODULES = [TemplateModule, JobSampleModule];
