import { Injectable } from '@nestjs/common';

import { Logger } from '@shared/modules/loggers/logger.service';
import { BaseQueueService } from '@shared/api/base-queue.service';
import { InjectQueue } from '@nestjs/bull';
import { QUEUES_CONFIG } from '@constants/queue.constant';
import { Queue } from 'bull';

@Injectable()
export class JobSampleService extends BaseQueueService {
    private logger = new Logger(JobSampleService.name);
    constructor(
        @InjectQueue(QUEUES_CONFIG.JOB_SAMPLE.QUEUE_NAME)
        protected readonly queue: Queue,
    ) {
        super(queue, JobSampleService.name);
    }

    async sampleFunction() {
        await this.addAsyncJob(QUEUES_CONFIG.JOB_SAMPLE.HANDLER_TEST, { data: 'data-sample' });
        return 'Add job success';
    }
    async sampleBatchFunction() {
        await this.addJobBatch({ name: QUEUES_CONFIG.JOB_SAMPLE.HANDLER_TEST_BATCH, data: 'data-sample-1' });
        return 'Add batch job success';
    }
}
