import { Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { JobSampleService } from '@modules/job-sample/job-sample.service';

@ApiTags('job-sample')
@Controller('job-sample')
export class JobSampleController {
    constructor(private readonly jobSampleService: JobSampleService) {}

    @Post()
    createJob() {
        return this.jobSampleService.sampleFunction();
    }

    @Post('/batch')
    createJobBatch() {
        return this.jobSampleService.sampleBatchFunction();
    }
}
