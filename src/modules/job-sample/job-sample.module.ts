import { Module } from '@nestjs/common';

import { JobSampleController } from './job-sample.controller';
import { JobSampleService } from './job-sample.service';
import { BaseServiceModule } from '@shared/api/service.module';
import { QUEUES_CONFIG } from '@constants/queue.constant';

@Module({
    imports: [
        BaseServiceModule.registerQueue({
            name: QUEUES_CONFIG.JOB_SAMPLE.QUEUE_NAME,
        }),
    ],
    controllers: [JobSampleController],
    providers: [JobSampleService],
})
export class JobSampleModule {}
