import { PaginationDto } from '@shared/api/dto/pagination.dto';

export class FilterTemplateDto extends PaginationDto {
    content?: string;
}
