import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { ErrorConstant } from '@constants/error.constant';

import * as exc from '@infra/exception/index';
import { Logger } from '@shared/modules/loggers/logger.service';

import { CreateTemplateDto } from './dto/create-template.dto';
import { UpdateTemplateDto } from './dto/update-template.dto';
import { TemplateService } from './template.service';
import { ApiOkResponsePayload, EApiOkResponsePayload } from '@shared/swagger';
import { Template } from '@models/schemas/Template.schema';
import { ObjectIDDto } from '@shared/dto/ObjectID.dto';
import { FilterTemplateDto } from '@modules/template/dto/filter-template.dto';

@ApiTags('template')
@Controller('template')
export class TemplateController {
    constructor(private readonly templateService: TemplateService) {}
    private logger = new Logger(TemplateController.name);

    @Post()
    @ApiOkResponsePayload(Template, EApiOkResponsePayload.OBJECT)
    create(@Body() createTemplateDto: CreateTemplateDto) {
        this.logger.info('This is log info');
        return this.templateService.create(createTemplateDto);
    }

    @Get()
    @ApiOkResponsePayload(Template, EApiOkResponsePayload.ARRAY)
    listALl() {
        this.logger.info('This is log info');
        return this.templateService.findAll();
    }

    @Get('/filter')
    @ApiOkResponsePayload(Template, EApiOkResponsePayload.ARRAY, true)
    filterAll(@Query() filterTemplateDto: FilterTemplateDto) {
        return this.templateService.filter(filterTemplateDto);
    }

    @Get(':id')
    @ApiOkResponsePayload(Template, EApiOkResponsePayload.OBJECT)
    findOne(@Param() params: ObjectIDDto) {
        return this.templateService.findOne(params.id);
    }

    @Patch(':id')
    @ApiOkResponsePayload(Template, EApiOkResponsePayload.OBJECT)
    update(@Param('id') id: string, @Body() updateTemplateDto: UpdateTemplateDto) {
        return this.templateService.update(id, updateTemplateDto);
    }

    @Delete(':id')
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    remove(@Param('id') id: string) {
        throw new exc.BadRequestException({
            message: ErrorConstant.TEMPLATE.NOT_FOUND.message,
            errorCode: ErrorConstant.TEMPLATE.NOT_FOUND.errorCode,
        });
    }
}
