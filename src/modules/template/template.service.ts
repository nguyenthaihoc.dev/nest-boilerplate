import { TemplateDocument } from '@models/schemas/Template.schema';
import TemplateRepository from '@models/repositories/Template.repository';
import { HttpStatus, Injectable } from '@nestjs/common';

import { ErrorConstant } from '@constants/error.constant';

import { BadRequestException } from '@infra/exception';
import { formatResponseSuccess } from '@shared/utils/format';

import { CreateTemplateDto } from './dto/create-template.dto';
import { UpdateTemplateDto } from './dto/update-template.dto';
import { ObjectIDDto } from '@shared/dto/ObjectID.dto';
import { Logger } from '@shared/modules/loggers/logger.service';
import { FilterTemplateDto } from '@modules/template/dto/filter-template.dto';

@Injectable()
export class TemplateService {
    private logger = new Logger(TemplateService.name);
    constructor(private templateRepository: TemplateRepository) {}
    async create(createTemplateDto: CreateTemplateDto) {
        const template = await this.templateRepository.templateDocumentModel.create({
            content: createTemplateDto.content,
        });
        return formatResponseSuccess({
            data: template,
            statusCode: HttpStatus.CREATED,
        });
    }

    async findAll(): Promise<TemplateDocument[]> {
        return this.templateRepository.templateDocumentModel.find().exec();
    }

    async filter(filterTemplateDto: FilterTemplateDto) {
        return this.templateRepository.listWithPagination(filterTemplateDto, { content: filterTemplateDto.content });
    }

    async findOne(id: ObjectIDDto['id']) {
        const template = await this.templateRepository.findTemplate(id);
        if (!template) {
            throw new BadRequestException({
                message: ErrorConstant.TEMPLATE.NOT_FOUND.message,
                errorCode: ErrorConstant.TEMPLATE.NOT_FOUND.errorCode,
            });
        }
        return template;
    }

    async update(id: string, updateTemplateDto: UpdateTemplateDto) {
        this.logger.info('Update all data', updateTemplateDto);
        const template = await this.templateRepository.findTemplate(id);
        if (!template) {
            throw new BadRequestException({
                message: ErrorConstant.TEMPLATE.NOT_FOUND.message,
                errorCode: ErrorConstant.TEMPLATE.NOT_FOUND.errorCode,
            });
        }
        return this.templateRepository.templateDocumentModel
            .updateOne(
                {
                    _id: id,
                },
                updateTemplateDto,
            )
            .exec();
    }

    remove(id: number) {
        return `This action removes a #${id} template`;
    }
}
