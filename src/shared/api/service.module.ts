import { DynamicModule, Module } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';

@Module({})
export class BaseServiceModule extends BullModule {
    static registerAsync(): DynamicModule {
        return {
            imports: [],
            module: BullModule,
            providers: [],
        };
    }
}
