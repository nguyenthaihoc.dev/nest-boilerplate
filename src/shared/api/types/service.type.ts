import { JobOptions } from 'bull';

export interface BaseServiceConfig {
    jobOptions: JobOptions;
}
