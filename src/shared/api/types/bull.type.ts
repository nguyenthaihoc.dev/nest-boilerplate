export interface IBaseQueueServiceBatchData {
    name?: string;
    data: any;
}
export interface IBaseQueueServiceData {
    data: any;
}
