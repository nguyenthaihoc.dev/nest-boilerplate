import _ from 'lodash';
import { DebouncedFunc } from 'lodash';
import { Logger } from '@shared/modules/loggers/logger.service';

const DEFAULT_BATCH_SIZE = 100;
const DEFAULT_BATCH_TIMEOUT = 5 * 1000;

export class BatchService<T> {
    protected batchItems: T[] = [];
    private readonly throttledFunc: DebouncedFunc<() => Promise<void>>;
    protected logger: Logger;

    constructor(
        private readonly batchFunc: any,
        serviceName: string,
        private readonly batchSize = DEFAULT_BATCH_SIZE,
        private batchTimeout = DEFAULT_BATCH_TIMEOUT,
    ) {
        this.logger = new Logger(serviceName);
        this.batchSize = batchSize;
        this.batchTimeout = batchTimeout;
        this.batchFunc = batchFunc;
        this.throttledFunc = _.throttle(this.execBatchFunc, batchTimeout, {
            leading: false,
            trailing: true,
        });
    }

    // Create an executor function
    public execBatchFunc = async () => {
        const tempBatchItems = _.clone(this.batchItems);
        this.batchItems = [];
        if (tempBatchItems.length === 0) {
            this.logger.info(`Processed ${tempBatchItems.length} items`);
            return;
        }
        await this.batchFunc(tempBatchItems);
        this.logger.info(`Processed ${tempBatchItems.length} items`);
    };

    async addItems(item: T) {
        this.batchItems.push(item);
        if (this.batchItems.length >= this.batchSize) {
            await this.throttledFunc.flush();
        } else {
            await this.throttledFunc();
        }
    }
}
