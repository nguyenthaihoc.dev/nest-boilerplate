import { BaseService } from './base.service';
import { Queue } from 'bull';
import { IBaseQueueServiceBatchData, IBaseQueueServiceData } from './types/bull.type';
import { BaseServiceConfig } from './types/service.type';
import { BatchService } from './batch.service';
import { Logger } from '@shared/modules/loggers/logger.service';

const baseServiceConfigDefault: BaseServiceConfig = {
    jobOptions: {
        delay: 0,
        attempts: 5,
        backoff: {
            type: 'fixed',
            delay: 1000 * 20,
        },
        removeOnComplete: { age: 60 * 60 * 2 },
        removeOnFail: false,
    },
};

export class BaseQueueService extends BaseService {
    private loggerBase: Logger;
    protected baseServiceConfig: BaseServiceConfig;
    protected batchService: BatchService<IBaseQueueServiceData>;
    protected handler: string;

    constructor(
        protected readonly queue: Queue,
        private serviceName: string,
        baseServiceConfig?: BaseServiceConfig,
        handler?: string,
    ) {
        super();
        this.loggerBase = new Logger(`${this.serviceName}QueueService`);
        this.baseServiceConfig = baseServiceConfig
            ? { ...baseServiceConfigDefault, ...baseServiceConfig }
            : baseServiceConfigDefault;
        this.batchService = new BatchService(this.addJobsBatch.bind(this), serviceName);
        this.handler = handler;
    }

    async addJobBatch(jobData: IBaseQueueServiceBatchData) {
        await this.batchService.addItems(jobData);
    }

    private async addJobsBatch(jobsData: IBaseQueueServiceBatchData[]) {
        try {
            await this.queue.addBulk(
                jobsData.map(jobData => ({
                    name: jobData.name || this.handler,
                    data: jobData,
                    opts: this.baseServiceConfig.jobOptions,
                })),
            );
        } catch (e) {
            this.loggerBase.error(e);
        }
    }

    public async addAsyncJob(handler: string, payload?: any) {
        const jobData: IBaseQueueServiceData = {
            data: payload,
        };
        await this.queue.add(handler, jobData, this.baseServiceConfig.jobOptions);
        this.loggerBase.info(`Added job ${handler} to queue`);
    }
}
