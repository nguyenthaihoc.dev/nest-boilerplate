import { Injectable } from '@nestjs/common';
import { Appender, configure, getLogger, Layout, Logger as Log4js } from 'log4js';
import { ClsServiceManager } from 'nestjs-cls';

const layouts: Record<string, Layout> = {
    console: {
        type: 'pattern',
        pattern: '%[%-6p %d [%c] | %m%]',
    },
    access: {
        type: 'pattern',
        pattern: '%[%-6p %d [%c] | rid:%x{rid} addr:%x{remoteAddr} %x{access}%]',
        tokens: {
            rid: function (logEvent) {
                let rid = logEvent.data.toString().split(' ', 1).pop();
                rid = rid.replace(/^.*:/, '');
                return rid;
            },
            remoteAddr: function (logEvent) {
                let remoteAddr = logEvent.data.toString().split(' ', 2).pop();
                remoteAddr = remoteAddr.replace(/^.*:/, '');
                remoteAddr = remoteAddr === '1' ? '127.0.0.1' : remoteAddr;
                return remoteAddr;
            },
            access: function (logEvent) {
                const [, ...data] = logEvent.data.toString().split(' ');
                data.pop();
                return data.join(' ');
            },
        },
    },
};

const appenders: Record<string, Appender> = {
    console: {
        type: 'console',
        layout: layouts.console,
    },
    consoleBasic: {
        type: 'console',
        layout: { ...layouts.console, type: 'basic' },
    },
    access: {
        type: 'console',
        layout: layouts.access,
    },
    accessBasic: {
        type: 'console',
        layout: { ...layouts.access, type: 'basic' },
    },
};

@Injectable()
export class LoggerService {
    /**
     * config logging
     * @example
     * Import Logging module
     * constructor(protected loggingService: LoggingService) {}
     * logger = this.loggingService.getLogger('serviceA');
     */
    constructor() {
        const level = process.env.LOG_LEVEL;
        const isLogWithColor = process.env.LOG_COLOR === 'true';
        configure({
            appenders: appenders,
            categories: {
                default: {
                    appenders: isLogWithColor ? ['console'] : ['consoleBasic'],
                    level: level,
                    enableCallStack: true,
                },
                access: {
                    appenders: isLogWithColor ? ['access'] : ['accessBasic'],
                    level: 'info',
                    enableCallStack: true,
                },
            },
        });
    }

    getLogger = getLogger;

    private _access = () => {
        const logger = this.getLogger('access');
        return {
            write: logger.info.bind(logger),
        };
    };

    logger = {
        default: getLogger('default'),
        access: this._access(),
    };
}

export class Logger {
    private logger: Log4js;
    constructor(category: string) {
        this.logger = new LoggerService().getLogger(category);
    }
    info(...args: any[]) {
        this.logger.info(`rid:${ClsServiceManager.getClsService().getId()}`, ...args);
    }
    debug(...args: any[]) {
        this.logger.debug(`rid:${ClsServiceManager.getClsService().getId()}`, ...args);
    }
    error(...args: any[]) {
        this.logger.error(`rid:${ClsServiceManager.getClsService().getId()}`, ...args);
    }
    warn(...args: any[]) {
        this.logger.warn(`rid:${ClsServiceManager.getClsService().getId()}`, ...args);
    }
}
