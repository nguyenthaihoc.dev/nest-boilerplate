import { Controller, Get } from '@nestjs/common';

import { ErrorConstant } from '@constants/error.constant';

import { AppService } from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }
    @Get('/error')
    getConstant() {
        return ErrorConstant;
    }

    // TODO: add health check
    @Get('/health')
    health(): string {
        return 'OK';
    }
}
