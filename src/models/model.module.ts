import { Template, TemplateSchema } from '@models/schemas/Template.schema';
import TemplateRepository from '@models/repositories/Template.repository';
import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

const repositories = [
    TemplateRepository,
];

@Global()
@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Template.name,
                schema: TemplateSchema,
            }
        ]),
    ],
    controllers: [],
    providers: [...repositories],
    exports: [...repositories],
})
export class ModelModule {}
