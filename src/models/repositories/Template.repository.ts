import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';

import { Template, TemplateDocument } from '@models/schemas/Template.schema';
import { BaseRepository } from '@shared/api/models/base.repository';

@Injectable()
export default class TemplateRepository extends BaseRepository<Template, TemplateDocument> {
    constructor(
        @InjectModel(Template.name)
        public templateDocumentModel: mongoose.PaginateModel<Template, mongoose.PaginateModel<TemplateDocument>>,
    ) {
        super(templateDocumentModel);
    }
    findTemplate(id: string): Promise<TemplateDocument> {
        return this.templateDocumentModel.findOne({ _id: id }).exec();
    }
}
