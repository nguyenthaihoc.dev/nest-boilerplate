export const ErrorConstant = Object.freeze({
    DEFAULT: {
        SUCCESS: {
            message: 'Success',
            errorCode: '000000',
        },
        FAILED: {
            message: 'Failed',
            errorCode: '000001',
        },
        VALIDATION: {
            message: 'Validation failed',
            errorCode: '000002',
        },
    },
    TEMPLATE: {
        NOT_FOUND: {
            message: 'Template not found',
            errorCode: '100001',
        },
    },
});
