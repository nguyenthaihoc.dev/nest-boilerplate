## Description

Please include a summary of the changes and a brief description about this PR

## Checklist before requesting a review

### Issue ticket number and link

- [ ] This PR has a valid ticket number or issue: [link]

### Testing & Validation

- [ ] This PR has been tested/validated.
- [ ] The code has been tested locally with test coverage match expectations.
- [x] Added new Unit/Component testing (Temporarily have not written unit tests).

### Security

- [ ] No secrets are being committed (i.e. credentials, PII)
- [ ] This PR does not have any significant security implications

### Code Review

- [ ] There is no unused functionality or blocks of commented out code (otherwise, please explain below)
- [ ] In addition to this PR, all relevant documentation (e.g. Confluence) and architecture diagrams (e.g. Miro) were updated